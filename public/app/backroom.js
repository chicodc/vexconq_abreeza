var app = angular.module('Backroom', ['ngAnimate','ui.bootstrap']);
var speaker = window.speechSynthesis,voice = window.speechSynthesis.getVoices(),worte = new SpeechSynthesisUtterance();


function saythis(artocall){
	worte.voice = voice[42];
	worte.rate = 1;
	var count = 0;
	artocall.forEach(function(element){
		count++;
		worte.text = worte.text+" please prepare A R number "+element.ar.split('').join(' ');
		console.log(element.ar);
		if(artocall.length === count){
			speaker.speak(worte);
		}
	});
}

function shutUp(){
	speaker.cancel();
}

app.controller('BackroomController',['$scope','$rootScope','$route', '$compile', '$uibModal', '$timeout', '$log', '$routeParams', '$location', 'generalFactory', 'socket', '$timeout',
	function($scope, $rootScope, $route, $compile, $uibModal, $timeout, $log, $routeParams, $location, generalFactory, socket, $timeout){

	function initbackcall(){
		$scope.PREPARE_AR = $scope.$parent.PREPARE_AR;
		generalFactory.getHandler('/backroom/forrelease', function(response){
			$scope.unprepared = response.unprepared;
			$scope.prepared = response.prepared;
			$scope.artocall = response.tobecalled;
			$scope.released = response.released;
			if($scope.artocall.length){
				worte.text = '';
				saythis($scope.artocall);
			}
		});
	};


	socket.on('newPendingAdded', function(){
		//socket.getSocket().removeAllListeners();
		initbackcall();
	});
	socket.on('arisready', function(){
		//socket.getSocket().removeAllListeners();
		initbackcall();
	});

	$scope.back = function(){
		$location.path('/menu');
	};

	$scope.logout = function(){
		generalFactory.getHandler('/logout', function(response){
			if(response.destroyed){
				 $location.path('/');
			}
		});
	};

	$scope.initback = function(){
		initbackcall();
	};

	$scope.prepare = function(id, parent_table){
		generalFactory.getHandler('/backroom/prepare'+ parent_table +'/' + id, function(response){
			socket.emit('arprepared');
			initbackcall();
			//$route.reload();
		});
	};


	$scope.$on('$destroy', function() {
		shutUp();
	})

}]);
