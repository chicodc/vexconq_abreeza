//'use strict';

angular.module('Qsystem', ['Main', 'Login', 'Frontline', 'Backroom', 'Ipadque', 'Quetv', 'Admin', 'Report','ngRoute', 'ngIdle'])

.config(function($routeProvider, $locationProvider,  IdleProvider, KeepaliveProvider){
	$routeProvider
		.when('/', {
			templateUrl : 'partials/login.html',
			controller : 'LoginController'
		})
		.when('/ipadque',{
			templateUrl : 'partials/ipadque.html',
			controller : 'IpadqueController'
		})
		.when('/quetv',{
			templateUrl : 'partials/quetv.html',
			controller : 'QuetvController'
		})
		.when('/queuing',{
			templateUrl : 'partials/main.html',
			controller : 'MainController'
		})
		.when('/quetvAdmin',{
			templateUrl : 'partials/quetvAdmin.html',
			controller : 'QuetvController'
		})
		.when('/iologin/:io', {
			templateUrl : 'partials/login.html',
			controller : 'IO_LoginController'
		})
		.when('/error', {
			templateUrl : 'partials/error.html'
		})
		.when('/quelist', {
			templateUrl : 'partials/quelist.html',
			controller 	: 'QuelistController'
		})
		.otherwise({
			redirectTo : '/'
		});

		$locationProvider.html5Mode(true);

		IdleProvider.idle(30 * 60); // in seconds
    	IdleProvider.timeout(5); // in seconds
    	KeepaliveProvider.interval(2); // in seconds
}
);
