module.exports = function (io) {


var socketCount = 0;

	io.on('connection', function(socket){

		var client = socket.request.connection;

	  	socketCount++;
	  	console.log('a user connected, socket count :'+socketCount+ ' '+client.remoteAddress+ ':' +client.remotePort+ ' : domain = '+ socket.handshake.xdomain +' : id '+socket.id);

		socket.on('onlogin', function(){
	       	console.log('user : '+client.remoteAddress+ ':' +client.remotePort +' is on the login page');
	    });
	  	socket.on('onfrontline', function(){
	       	console.log('user : '+client.remoteAddress+ ':' +client.remotePort +' is on the frontline page');
	    });
	  	socket.on('disconnect', function(){
	  		socketCount--;
	  		var id = this.id;
	  		//users.disconnectUser(client.remoteAddress);
	      console.log('user disconnected , socket count :'+socketCount + ': id = '+ id);
	    });
	  	socket.on('newPendingRequest', function(numberque) {
	  		console.log('a new client request');
		    socket.broadcast.emit('newPendingAdded',numberque);
		});
	  	socket.on('refreshQuetv', function(){
	  		socket.broadcast.emit('reloadQuetv');
	  	});
	  	socket.on('callClient', function(data){
	  		socket.broadcast.emit('calledClient', {data : data});
	  	});
	  	socket.on('arprepared', function() {
	  		console.log('a new ar is prepared');
		    socket.broadcast.emit('arisready');
		});
	});




};
