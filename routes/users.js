var connection = require('./conn.js');
var crypto = require('crypto');
var sha1 = require('sha1');

function add_tolog(name,username,account_type,server_ip,event,remark){
	var LogData = {
        name 	   : name,
        user_name : username,
        account_type : account_type,
        server_ip : server_ip,
        event : event,
        remark : remark
    };
	connection.query("INSERT INTO logs set ? ", LogData, function(err, results) {
		if(err){ console.log('ERROR on users.js add_tolog : %s', err); }
	});
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

module.exports.verify = function(req , res){

	var ip = req.connection.remoteAddress;
    var user = JSON.parse(JSON.stringify(req.body));

    connection.query('SELECT id, firstname, lastname, username, account_type, activated, level, a.change , logged, ip FROM users a WHERE username = ? AND password = sha1(?)', [user.username, user.password], function(err, results) {
			if(err){
				console.log('ERROR on users.js verify : %s', err);
			}else{
			 if (results.length){
			 	if(results[0].activated =='Yes'){
					  if(results[0].logged != '1'){
							  if(results[0].change != 0){
								  //Log activity
								  add_tolog(results[0].firstname + ' ' + results[0].lastname,results[0].username, results[0].account_type, req.connection.remoteAddress, "Log entry", 'Successfully logged in!!!');

								  //Set Sessions (note: in angular access through sevice only)
								  req.session.ip = req.connection.remoteAddress;
								  req.session.useragent = req.headers['user-agent']	;
								  req.session.userdata = results[0];
								  req.session.loggedin = true;
								  req.session.userid = results[0].id;
								  req.session.username = results[0].username;
								  req.session.accountype = results[0].account_type;
								  req.session.name = results[0].firstname + ' ' + results[0].lastname.charAt(0).toUpperCase()+'.';
								  req.session.lname = results[0].lastname;
								  req.session.level = results[0].level;

								  //important for que initialization
								  req.session.status = 'all';
								  req.session.jump = 0;

							  	  //SAVE Session
								  req.session.save(function(err) {
									  	console.log('SESSION SAVED');
								      	if (err) {
								      		console.log('error in saving session: ' + err);
								      	}else{
								      	  connection.query('UPDATE users SET logged = 1, ip = ? WHERE id = ?', [ip, results[0].id], function(error, result){
														if(error){
															console.log('ERROR on users.js verify : %s', error);
														}
													});
									      console.log(req.session.loggedin);
									      var response = { success: results[0], change : results[0].change };
										  	res.send(response);
								      	}
							      });
						      }else{
						      	if (err) {
						      		console.log('error in saving session: ' + err);
						      	}else{

								  req.session.change = results[0].change;
							      console.log(req.session.loggedin);
							      var response = { success: 'unchanged', change : results[0].change };
								  res.send(response);
						      	}
						      }
					}else{
						add_tolog(results[0].firstname + ' ' + results[0].lastname, user.username, results[0].account_type, req.connection.remoteAddress, "Log entry", 'Attemp to login but already logged in');
				  		console.log(req.session.loggedin);
				      	var response = { error: err, message: 'You are already logged in the system from '+ results[0].ip +'. Please contact the system admin if you are experiencing trouble.'};
					  	res.send(response);
					}
			     }else{
			     	add_tolog(results[0].firstname + ' ' + results[0].lastname, user.username, results[0].account_type, req.connection.remoteAddress, "Log entry", 'Attemp to login but not yet Activated');
				  	console.log(req.session.loggedin);
				      var response = { error: err, message: 'Account not yet activated. Please contact your supervisor or the system admin.'};
					  res.send(response);
			     }
			  }else{
				  add_tolog('UNKNOWN', user.username, 'UNKNOWN', req.connection.remoteAddress, "Log entry", 'Attemp to login without proper access');

				  var response = { error: err, message: 'Wrong username and password' };
			      res.send(response);
			  }
			}
	});
};


module.exports.sessioncheck = function(req , res){
		var response = {};
		var id = req.session.userid;
		connection.query('SELECT * FROM users WHERE id = ? AND logged = 1 ', [req.session.userid], function(err, results){
			if(err){
				console.log('ERROR on sessioncheck : %s', err);
			}else{
				if(results.length){
					 if (req.session.loggedin || req.session.loggedin != undefined){
					 	  //req.session.cookie.maxAge = new Date(Date.now() + 3600000);
					 	  //req.session.cookie.expires = new Date(Date.now() + 3600000);
					 	  console.log("logged "+ req.session.loggedin);
						  response = { loggedin: req.session.loggedin , type : req.session.accountype, userid : req.session.userid, level : req.session.level, name : req.session.name};
						  res.send(response);
					  }else{
					  	connection.query('UPDATE queserve SET status = NULL, active = NULL, sit = NULL, servicing = NULL, agent_id = NULL WHERE agent_id = ?', [id] , function(error, result){
							if(error){
								console.log('ERROR on logout : %s', error);
							}
						});

					  	  console.log("unlogged "+ req.session.loggedin);
					  	  response = { loggedin: false};
					  	  req.session.destroy();
					      res.send(response);
					  }
				}else{
					connection.query('UPDATE queserve SET status = NULL, active = NULL, sit = NULL, servicing = NULL, agent_id = NULL WHERE agent_id = ?', [id] , function(error, result){
						if(error){
							console.log('ERROR on logout : %s', error);
						}
					});
					 console.log("unlogged "+ req.session.loggedin);
					 response = { loggedin: false};
					 req.session.destroy();
					 res.send(response);
				}
			}
		});
};


module.exports.changepass = function(req, res){
    var user = JSON.parse(JSON.stringify(req.body));
	connection.query('UPDATE users a SET password = ?, a.change = 1 WHERE username = ?',[sha1(user.password), user.username],function(err, result){
		if(err){
			console.log('ERROR : %s', err);
		    var response = {error : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	add_tolog('UNKNOWN',user.username, 'UNKNOWN', req.connection.remoteAddress, "Log entry", 'Changed password.');

	     	var response = {success : 'Update successful' , name : req.session.name};
	     	res.send(response);
	     }
	});
};

module.exports.handle = function(req, res){
	var id = req.session.userid;
	connection.query('SELECT q.agent_id, q.counter_number, q.status, q.sit, q.servicing, q.available, s.service FROM queserve q \
						LEFT JOIN status s on q.servicing = s.id \
						WHERE agent_id = ?',  [id], function(err, results) {
		if(err){
			console.log('Error on handle select : %s', err);
		}else{
			if(results.length){
				var response = {success : results[0] , availability : results[0].available , name : req.session.name};
				req.session.transaction = results[0].servicing;
				req.session.counter = results[0].counter_number;
				res.send(response);
			}
		}
	});
};


module.exports.updateQue = function(req, res){
	var id = req.session.userid;

	connection.query('SELECT a.user_id, a.counter, a.type, b.firstname, b.lastname FROM users_handle a LEFT JOIN users b ON b.id = a.user_id WHERE a.user_id = ?',  [id],function(err, results) {
		if(err){
			console.log('ERROR on users.js updateQue : %s', err);
		}else{
				if(results.length){
		    	var name = results[0].firstname + ' ' + results[0].lastname.charAt(0).toUpperCase()+'.';
		    	connection.query('UPDATE queserve SET sit = ? , servicing = ? WHERE counter_number = ?', [name, results[0].type, results[0].counter], function(error, rows){
		    		if(error){
				     	var response = {success : 'ERROR' , name : req.session.name};
				     	res.send(response);
				     }else{
				     	var response = {success : 'Update successful' , name : req.session.name};
				     	//req.session.transaction = results[0].type;
				     	//req.session.counter = results[0].counter;
				     	res.send(response);
				     }
		    	});

		     }
			}
	});
};

module.exports.changeHandle = function(req, res){
	var id = req.session.userid;
	var name = req.session.name;
	var data = JSON.parse(JSON.stringify(req.body));
	var query = '';
	var newData;
	connection.query('SELECT id FROM pending WHERE flag = '+ id +' UNION ALL SELECT id FROM skipped WHERE flag = '+ id +'', function(check_err, check_result){
		if(check_err){
			console.log('Changehandle error : %s', check_err);
		}else{
			if(!check_result.length){
				connection.query('SELECT user_id FROM users_handle WHERE user_id = ?', [id], function(handle_err, handle_result){
					if(!handle_result.length){
						query = 'INSERT INTO users_handle(user_id, counter, type) VALUES (?,?, ?)';
						newData = [id, data.counter, data.transaction];
					}else{
						query = 'UPDATE users_handle SET counter = ?, type = ? WHERE user_id = ?';
						newData = [data.counter, data.transaction, id];
					}
					connection.query(query, newData ,function(updateHandle_err, updateHandle_results) {
						if(updateHandle_err){
							console.log('updateHandle_err : %s ', updateHandle_err);
						}
					     connection.query('UPDATE queserve SET sit = NULL, servicing = NULL, agent_id = NULL WHERE agent_id = ?',  [id],function(clear_err, clear_results) {
					     	if(clear_err){
							    console.log('clear_err : %s', clear_err);
							}
					     	connection.query('UPDATE queserve SET sit = ?, servicing = ? , available = 0, agent_id = ? WHERE counter_number = ? AND agent_id IS NULL',  [name, data.transaction, id, data.counter],function(updateQuserve_err, updateQuserve_results) {
						     console.log('fgsdfgsdfgdfs' + updateQuserve_err);
							     if(updateQuserve_err){
							     	console.log('queserve : %s', updateQuserve_err);
							     	var response = {success : 'ERROR' , name : req.session.name};
							     	res.send(response);
							     }else{
							     	var response = {success : 'Update successful' , name : req.session.name};
							     	req.session.transaction = data.transaction;
							     	req.session.counter = data.counter;
							     	res.send(response);
							     }
							 });
						});
					});
				});
			}else{
				var response = {success : 'ACTIVE' , name : req.session.name, message : 'Please finish serving client first!'};
				res.send(response);
			}
		}
	});
};

module.exports.clearHandle = function(req, res){
	var id = req.session.userid;
	connection.query('SELECT id FROM pending WHERE flag = '+ id +' UNION ALL SELECT id FROM skipped WHERE flag = '+ id +'', function(check_err, check_result){
		if(check_err){
			console.log('ERROR on clearHandle check: %s', check_err);
		}else{
			if(!check_result.length){
				connection.query('UPDATE queserve SET status = NULL, active = NULL, sit = NULL, servicing = NULL, available = 0,  agent_id = NULL WHERE agent_id = ?', [id] , function(err, result){
					if(err){
						console.log('ERROR on logout : %s', err);
					}else{
						req.session.transaction = null;
						req.session.counter = null;
						var response = {success : 'CLEARED', name : req.session.name, message: 'Counter cleared.'};
						res.send(response);
					}
				});
			}else{
				var response = {success : 'ACTIVE', name : req.session.name, message : 'Please finish serving client first!'};
				res.send(response);
			}
		}
	});
};

module.exports.togglebreak = function(req, res){
	connection.query('SELECT counter FROM users_handle WHERE user_id = ?', [req.session.userid], function(errors, result){
		if(errors){
			console.log('ERROR on users.js togglebreak : %s', errors);
		}else{
			if(result.length){
			connection.query('SELECT available FROM queserve WHERE counter_number = ?',  [result[0].counter],function(error, results) {
			    if(results.length){
			    	var availability = results[0].available;
			    	if(availability != '1'){
			    		availability = '1';
			    	}else{
			    		availability = '0';
			    	}
				  	connection.query("UPDATE queserve SET available='"+availability+"' WHERE counter_number= ?", [result[0].counter] , function(err, rows){
				  		if (err) {console.log("Error updating : %s ",err );}
				  		if(err){
					     	var response = {success : 'ERROR' , name : req.session.name};
					     	res.send(response);
					     }else{
					     	var response = {success : 'Update successful' , name : req.session.name};
					     	res.send(response);
					     }
				  	});

			     }
			});
			}
		}
	});
};



module.exports.getsettings = function(req, res){
	var userid = req.session.userid;
	connection.query('SELECT a.id, firstname, lastname, username, account_type, activated, a.level, b.name as level_name FROM users a LEFT JOIN user_level b ON a.level = b.id WHERE a.id != '+ userid +'', function(err, results){

	var newArray = [];
	var i = 0;
	if(err){
		console.log('ERROR on users.js getsettings : %s', err);
	}else{
		if(results.length){

			results.forEach(function(element){

				connection.query('SELECT b.id, b.permission, a.user_id, a.permission_id FROM user_permissions b LEFT JOIN users_permission a ON b.id = a.permission_id AND a.user_id = '+ element.id +' WHERE b.level >= '+ element.level +' ORDER BY b.id ASC',
				function(error, result){

					if(error){

						console.log("Error updating : %s ",error );
		    			var response = {err : error };
		    			res.send(response);

					}else{

						element['permissions'] = result;
						connection.query('SELECT b.id, b.menu_name, b.parent, a.user_id, a.menu_id FROM user_menu b LEFT JOIN users_menu a ON b.id = a.menu_id AND a.user_id = '+ element.id +' WHERE b.level >= '+ element.level +' AND b.disabled != 1 ORDER BY b.id ASC',
						function(err_sub, result_menu){

							if(err_sub){

								console.log("Error updating : %s ",err_sub );
		    					var response = {err : err_sub };
		    					res.send(response);

							}else{

								i++;
								element['menus'] = result_menu;
								newArray.push(element);

								if(i === results.length){

									var response = {success : newArray };
									res.send(response);

								}

							}

						});

					}

				});

			});

		}else{

			var response = {err : err };
			console.log(err);
		    res.send(response);

		}
	}
	});
};



module.exports.getpermissions = function(req, res){
	connection.query('SELECT id, firstname, lastname, username, account_type, activated, a.level FROM users a', function(err, results){
		if(err){
			console.log('ERROR on users.js verify : %s', err);
		}else{
			var newArray = [];
			var i = 0;
			if(results.length){
				results.forEach(function(element){

					connection.query('SELECT b.id, b.permission, a.user_id, a.permission_id FROM user_permissions b LEFT JOIN users_permission a ON b.id = a.permission_id AND a.user_id = '+ element.id +' WHERE b.level >= '+ element.level +' ORDER BY b.id ASC',
						function(error, result){
						if(error){
							console.log("Error updating : %s ",error );
					     	var response = {err : error };
				     		res.send(response);
						}else{
							element['permissions'] = result;
							newArray.push(element);
							i++;
							if(i === results.length){
								var response = {success : newArray };
								res.send(response);
							}
						}
					});
				});
			}else{
				var response = {err : err };
				console.log(err);
	     		res.send(response);
			}
		}
	});
};

module.exports.getmenus = function(req, res){
	connection.query('SELECT id, firstname, lastname, username, account_type, activated, a.level FROM users a', function(err, results){
		if(err){
			console.log('ERROR on users.js getmenus : %s', err);
		}else{
			var newArray = [];
			var i = 0;
			if(results.length){
				results.forEach(function(element){
					//start

					connection.query('SELECT b.id, b.menu_name, b.parent, a.user_id, a.menu_id FROM user_menu b LEFT JOIN users_menu a ON b.id = a.menu_id AND a.user_id = '+ element.id +' WHERE b.level >= '+ element.level +' AND b.disabled != 1 ORDER BY b.id ASC',
					function(err_sub, result_menu){

						if(err_sub){

							console.log("Error updating : %s ",err_sub );

						    var response = {err : err_sub };
						    res.send(response);

						}else{
							i++;

							element['menus'] = result_menu;

							newArray.push(element);

							if(i === results.length){
								var response = {success : newArray };
								res.send(response);
							}
						}
					});
					//end
				});
			}else{
				var response = {err : err };
				console.log(err);
	     		res.send(response);
			}
		}
	});
};


module.exports.getlevels = function(req, res){
	connection.query('SELECT id,name FROM user_level ORDER BY id DESC', function(err, result){
		if(err){
			console.log("Error updating : %s ",err );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
		}else{
			var response = {success : result , name : req.session.name};
			res.send(response);
		}
	});

};

module.exports.setpermissions = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	var permissions = Object.keys(data.permissions).map(function(k) { if(data.permissions[k][0] != null || data.permissions[k][0] != 'NULL'){return data.permissions[k];} });

	connection.query('DELETE FROM users_permission WHERE user_id = ?', [data.user_id], function(error, result){
		if(error){
			console.log("Error updating : %s ",error );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
		}else{
			connection.query('INSERT INTO users_permission(permission_id, user_id) VALUES ?', [permissions] ,function(err, results){
				if(err){
					console.log("Error updating : %s ",err );
			     	var response = {success : 'ERROR' , name : req.session.name, message : 'Error on data save.'};
			     	res.send(response);
				}else{
					var response = {success : 'SUCCESS' , name : req.session.name, message : 'Data saved successfully.'};
			     	res.send(response);
				}
			});
		}
	});
};


module.exports.setmenus = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	var menus = Object.keys(data.menus).map(function(k) { if(data.menus[k][0] != null || data.menus[k][0] != 'NULL'){return data.menus[k];} });

	connection.query('DELETE FROM users_menu WHERE user_id = ?', [data.user_id], function(error, result){
		if(error){
			console.log("Error updating : %s ",error );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
		}else{
			connection.query('INSERT INTO users_menu(menu_id, user_id) VALUES ?', [menus] ,function(err, results){
				if(err){
					console.log("Error updating : %s ",err );
			     	var response = {success : 'ERROR' , name : req.session.name, message : 'Error on data save.'};
			     	res.send(response);
				}else{
					var response = {success : 'SUCCESS' , name : req.session.name, message : 'Data saved successfully.'};
			     	res.send(response);
				}
			});
		}
	});
};


module.exports.setLevel = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	connection.query('UPDATE users SET level = ? WHERE id = ?', [data.level, data.user_id], function(err, result){
		if(err){
			console.log("Error updating : %s ",err );
			var response = {success : 'ERROR' , name : req.session.name, message : 'Error on data save.'};
			res.send(response);
		}else{
			var response = {success : 'SUCCESS' , name : req.session.name, message : 'Data saved successfully.'};
			res.send(response);
		}
	});
};

module.exports.adduser = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));

	var RecordDetailParsedFromForm = {
		firstname 	: data.firstname.capitalize(),
		lastname 	: data.lastname.capitalize(),
		username 	: data.username,
		password 	: sha1(data.password),
		level: data.level,
		activated 	: 'No',
		change 		: data.change
	};
	connection.query('SELECT * FROM users WHERE username = ?', [data.username],function(error, results){
		if(error){
			console.log('ERROR on users.js adduser : %s', error);
		}else{
			if(!results.length){
				connection.query('INSERT INTO users SET ?', RecordDetailParsedFromForm,function(err, result){
					if(err){
			  			console.log("Error updating : %s ",err );
				     	var response = {success : 'ERROR' , name : req.session.name, message : 'Error on data save'};
				     	res.send(response);
				     }else{
				     	connection.query('SELECT MAX(id) as num FROM users', function(errors, res){
				     		if(errors){
				     			console.log("Error updating : %s ",errors );
				     		}
				     		if(data.level > 2){
				     			connection.query('INSERT INTO users_menu(user_id, menu_id) VALUES ('+res[0].num+', 1)', function(menu_error, menu_result){
				     				if(menu_error){
						     			console.log("Error updating menu for staff: %s ",menu_error );
						     		};
				     			});
				     		}else{
				     			connection.query('INSERT INTO users_menu(user_id, menu_id) VALUES ('+res[0].num+', 1), ('+res[0].num+', 2), ('+res[0].num+', 3), ('+res[0].num+', 4), ('+res[0].num+', 5)', function(menu_errors, menu_results){
				     				if(menu_errors){
						     			console.log("Error updating menu for supervisor/admin: %s ",menu_errors );
						     		};
				     			});
				     		}
				     	});


				     	var response = {success : 'Insert successful' , name : req.session.name, message : 'USER ADDED SUCCESSFULLY'};
				     	res.send(response);
				     }
				});
			}else{
				var response = {success : 'ERROR' , name : req.session.name, message : 'USERNAME ALREADY IN USE'};
				res.send(response);
			}
		}
	});

};

module.exports.activate = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	connection.query('UPDATE users SET activated = ? WHERE id = ?', [data.status,data.id],function(err, result){
		if(err){
  			console.log("Error updating : %s ",err );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	var response = {success : 'SUCCESS' , name : req.session.name};
	     	res.send(response);
	     }
	});
};


module.exports.resetpass = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	connection.query('UPDATE users SET password = ? WHERE username = ?', [sha1(data.password), data.username], function(err, result){
		if(err){
  			console.log("Error updating : %s ",err );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	var response = {success : 'SUCCESS' , name : req.session.name};
	     	res.send(response);
	     }
	});
};

module.exports.promote = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	connection.query('UPDATE users SET account_type = ? WHERE id = ?', [data.level,data.id],function(err, result){
		if(err){
  			console.log("Error updating : %s ",err );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	var response = {success : 'Update successful' , name : req.session.name};
	     	res.send(response);
	     }
	});
};

module.exports.deleteuser = function(req, res){
	var id = req.params.id;

	connection.query('DELETE FROM users_menu WHERE user_id = ?', [id], function(err, res){ if(err){console.log('ERROR : %s', err);} });
	connection.query('DELETE FROM users_permission WHERE user_id = ?', [id], function(err, res){ if(err){console.log('ERROR : %s', err);} });
	connection.query('DELETE FROM users_handle WHERE user_id = ?', [id], function(err, res){ if(err){console.log('ERROR : %s', err);} });
	connection.query('DELETE FROM users WHERE id = ?', [id],function(err, result){
		if(err){
  			console.log("Error updating : %s ",err );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	var response = {success : 'Delete successful' , name : req.session.name};
	     	res.send(response);
	     }
	});
};

module.exports.getusermenu = function(req, res){
	var user_id = req.session.userid;
	connection.query('SELECT b.menu_name, b.parent, b.icon , a.user_id, a.menu_id  FROM user_menu b LEFT JOIN users_menu a ON b.id = a.menu_id WHERE a.user_id = '+ user_id +' AND b.disabled != 1 AND b.parent IS NULL OR b.parent = NULL', function(err, result){
		var newArray = [];
		var i = 0;
		if(err){
			console.log('ERROR on getusermenu : %s', err);
		}else{
			if(result.length){
				result.forEach(function(element){
				 	connection.query('SELECT b.menu_name, b.parent , b.icon, a.user_id, a.menu_id  FROM user_menu b LEFT JOIN users_menu a ON b.id = a.menu_id WHERE a.user_id = '+ user_id +' AND b.disabled != 1 AND b.parent = '+ element.menu_id, function(err_sub, result_sub){
						i++;
						element['sub'] = result_sub;
						newArray.push(element);
						if(i === result.length){
							res.send(newArray);
						}
					});
				});
			}
		}
	});
};

module.exports.getuserpermission = function(req, res){
	var user_id = req.session.userid;

	connection.query('SELECT b.id, b.permission, a.user_id, a.permission_id FROM user_permissions b LEFT JOIN users_permission a ON b.id = a.permission_id AND a.user_id = '+ user_id +'  ORDER BY b.id ASC',
		function(error, result){
		if(error){
			console.log("Error updating : %s ",error );
	     	var response = {err : error };
     		res.send(response);
		}else{
			var response = {success : result };
			res.send(response);
		}
	});
};


module.exports.logout = function(req , res){
	var id  = req.session.userid;
	connection.query('SELECT id FROM pending WHERE flag = '+ id +' UNION ALL SELECT id FROM skipped WHERE flag = '+ id +'', function(check_err, check_result){
		if(check_err){
			console.log('Changehandle erro : %s', check_err);
		}else{
			if(!check_result.length){
				connection.query('UPDATE queserve SET status = NULL, active = NULL, sit = NULL, servicing = NULL, agent_id = NULL WHERE agent_id = ?', [id] , function(err, result){
					if(err){
						console.log('ERROR on logout : %s', err);
					}
				});
				connection.query('UPDATE users SET logged = 0, ip = NULL WHERE id = ?', [id], function(update_logout_err, update_logout_result){
					if(update_logout_err){
						console.log('ERROR on logout : %s', update_logout_err);
					}
				});
				req.session.destroy(function(){});
				var response = { destroyed: true };
				res.send(response);
			}else{
				var response = {destroyed : false, success : 'ACTIVE'};
				res.send(response);
			}
		}
	});
};

module.exports.disconnectUser = function(ip){
	connection.query('SELECT * FROM users WHERE ip = ?', [ip], function(err, result){
		if(err){
			console.log('Error on disconnect : %s', err);
		}
		if(result.length){
			connection.query('UPDATE users SET logged = 0, ip = NULL WHERE ip = ?', [ip], function(up_err, up_result){
				if(up_err){
					console.log('Error on disconnect update : %s', up_err);
				}
				if(up_result){
					console.log(result[0].username+' has disconnected.');
				}
			});
		}
	});
};

module.exports.getMyCounter = function(req, res){
	var counter_number = req.session.counter;
	var current_table = req.session.current_table;
	var id = req.session.current;
	if(counter_number != undefined){
		connection.query("SELECT * FROM "+ current_table +" WHERE id = ?", [id], function(error, result){
			if(error){
				console.log('ERROR on getMyCounter : %s', error);
			}else{
				var RecordDetailParsedFromForm = {
		        client_name : result[0].alias,
		        quecard		: result[0].quecard,
		        status 		: result[0].status,
		        date_of_transaction : result[0].date_of_transaction
		    };
		    var prefix = {
				    	1 : "APPOINTMENT",
						2 : "REPAIR/CHECKUP",
						3 : "RELEASING",
						4 : "COURTESY LANE"
					};
		    var quedata = {
		        banner_num 		: counter_number,
		       banner_name 		: "<div><status-font style='font-size: 65px;'>"+prefix[RecordDetailParsedFromForm.status] + "</status-font></div> <div style='margin-top: 0px;'><quecard-font style='font-size: 130px;'>" +result[0].quecard+"</quecard-font></div>",
		        date_of_transaction : result[0].date_of_transaction
		    };

				connection.query("UPDATE quedata SET ? WHERE id=1",  [quedata], function(clrData_err, clrData_result){
					if (clrData_err) {console.log("Error updating quedata on getMyCounter: %s ",clrData_err );}else {
						var response = {counter : counter_number};
						res.send(response);
					}
				});
			}
		 });
	 }else{
	 	console.log('counter_number is undefined. something wrong with the session %s', util.inspect(req.session, {showHidden: false, depth: null}));
	 }
};


module.exports.logoutUser = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	var id  = data.id;
	connection.query('SELECT id FROM pending WHERE flag = '+ id +' UNION ALL SELECT id FROM skipped WHERE flag = '+ id +'', function(check_error, check_result){
		if(check_error){
			console.log('ERROR on logoutUser : %s', check_error);
		}else{
			if(check_result.length){
				var response = {success : 'ACTIVE' , name : req.session.name};
				res.send(response);
			}else{
				connection.query('UPDATE users SET logged = 0, ip = NULL WHERE id = ? AND username = ?', [data.id, data.username], function(error, result){
					if(error){
						console.log('ERROR on force logout : %s', error);
						var response = {success : 'ERROR' , name : req.session.name};
				     	res.send(response);
				     }else{
				     	var response = {success : 'SUCCESS' , name : req.session.name};
				     	res.send(response);
					}
				});
			}
		}
	});
};
