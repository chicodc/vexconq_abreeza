var connection = require('./conn.js');
var util = require('util');

function add_tolog(name,username,account_type,server_ip,event,remark){
	var LogData = {
        name 	   : name,
        user_name : username,
        account_type : account_type,
        server_ip : server_ip,
        event : event,
        remark : remark
    };
	connection.query("INSERT INTO logs set ? ", LogData, function(err, results) {});
};

module.exports.clearall = function(req, res){
	connection.query('SELECT * FROM latest_clear WHERE DATE(date) = CURDATE()', function(error, results){
		if(error){
  			console.log("Error updating : %s ",error );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	if(!results.length){
	     		/*
	     		 * CLEAR CLIENTS
	     		 */
				connection.query("DELETE FROM pending WHERE DATE(date_of_transaction) != CURDATE()");
				connection.query("DELETE FROM skipped WHERE DATE(date_of_transaction) != CURDATE()");

				/*
				 * MOVE SERVED TO ARCHIVE
				 */
				connection.query("SELECT * FROM served WHERE DATE(date_of_transaction) != CURDATE()", function(err, result){
					for( var i = 0, len = result.length; i < len; i++ ) {
						var data = {
						        user_id    	: result[i].user_id,
						        numberque 	: result[i].numberque,
						        quecard		: result[i].quecard,
						        salutation 	: result[i].salutation,
						        firstname 	: result[i].firstname,
						        lastname 	: result[i].lastname,
						        status 		: result[i].status,
						        agent 		: result[i].agent,
						        date_of_transaction : result[i].date_of_transaction,
						        time_called : result[i].time_called,
						        time_served : result[i].time_served ,
						        time_on_counter : result[i].time_on_counter,
						        ar          : result[i].ar,
						        alias		: result[i].alias
						    };

						connection.query("INSERT INTO served_archive SET ? ", [data]);
					}
					connection.query("DELETE FROM served WHERE DATE(date_of_transaction) != CURDATE()");
				});


				/*
				 * DISCONNECT USERS
				 */
				connection.query('UPDATE users SET logged = 0, ip = NULL WHERE DATE(date_updated) != CURDATE()', function(up_err, up_result){
					if(up_err){
						console.log('Error on disconnect update : %s', up_err);
					}
				});
				connection.query("UPDATE quedata SET banner_num = NULL, banner_name = NULL WHERE DATE(date_of_transaction) != CURDATE()", function(error, result){
					if(error){console.log('ERROR on quedata clear : %s', error);}
				});
				connection.query("UPDATE queserve SET client_name = NULL , status = NULL, active = NULL, sit = NULL, available = 0, servicing = NULL, agent_id = NULL, numberque = NULL  WHERE DATE(date_of_transaction) != CURDATE()", function(error, result){
					if(error){console.log('ERROR on queserve clear : %s', error);}
				});

				/*
				 * DESTROY SESSION AND UPDATE LATEST CLEARING
				 */
				connection.query("UPDATE latest_clear SET date = CURRENT_TIMESTAMP()");

		     	var response = {success : 'Update successful' , name : req.session.name};
		     	res.send(response);

	     		req.session.destroy(function(){});

			}else{
				var response = {success : 'Already cleared' , name : req.session.name};
		     	res.send(response);
			}
	     }

	});
};


module.exports.verify = function(req, res){
	var user = JSON.parse(JSON.stringify(req.body));

    connection.query('SELECT id, firstname, lastname, username, account_type, activated, level, a.change , logged, ip FROM users a WHERE username = ? AND password = sha1(?)', [user.username, user.password], function(err, results) {
    	if(err){
    		console.log('ERROR on system.js verify for iologin : %s', err);
    	}else{
			 if (results.length){
			 	if(results[0].logged != 0){
			 		add_tolog(results[0].firstname + ' ' + results[0].lastname, user.username, results[0].account_type, req.connection.remoteAddress, "Log entry", 'Attemp to login but already logged in');
			  		console.log(req.session.loggedin);
			      	var response = { error: err, message: 'This user is already logged in the FRONTLINE from '+ results[0].ip +'. Please use a different user or please contact the system admin if you are experiencing trouble.'};
				  	res.send(response);
			 	}else{
			 		if(results[0].activated =='Yes'){
							  if(results[0].change != 0){
								  //Log activity
								  add_tolog(results[0].firstname + ' ' + results[0].lastname,results[0].username, results[0].account_type, req.connection.remoteAddress, "Log entry", 'IO: Successfully logged in!!!');

								  //Set Sessions (note: in angular access through sevice only)
								  req.session.ioip = req.connection.remoteAddress;
								  req.session.iouseragent = req.headers['user-agent'];
								  req.session.iouserdata = results[0];
								  req.session.iologgedin = true;
								  req.session.iouserid = results[0].id;
								  req.session.iousername = results[0].username;
								  req.session.ioaccountype = results[0].account_type;
								  req.session.ioname = results[0].firstname + ' ' + results[0].lastname.charAt(0).toUpperCase()+'.';
								  req.session.iolname = results[0].lastname;
								  req.session.iolevel = results[0].level;

								  //important for que initialization
								  req.session.iostatus = 'all';
								  req.session.iojump = 0;

							  	  //SAVE Session
								  req.session.save(function(err) {
									  	console.log('SESSION SAVED');
								      	if (err) {
								      		console.log('error in saving session: ' + err);
								      	}else{
									      var response = { success: results[0], change : results[0].change };
										  res.send(response);
								      	}
							      });
						      }else{
						      	if (err) {
						      		console.log('error in saving session: ' + err);
						      	}else{

								  req.session.iochange = results[0].change;
							      console.log(req.session.iologgedin);
							      var response = { success: 'unchanged', change : results[0].change };
								  res.send(response);
						      	}
						      }
			     	}else{
			     		add_tolog(results[0].firstname + ' ' + results[0].lastname, user.username, results[0].account_type, req.connection.remoteAddress, "Log entry", 'Attemp to login but not yet Activated');
				  		console.log(req.session.iologgedin);
				      	var response = { error: err, message: 'Account not yet activated. Please contact your supervisor or the system admin.'};
					  	res.send(response);
			     	}
			    }
		  	}else{
			  add_tolog('UNKNOWN', user.username, 'UNKNOWN', req.connection.remoteAddress, "Log entry", 'Attemp to login without proper access');

			  var response = { error: err, message: 'Wrong username and password' };
		      res.send(response);
		  	}

		}
	});
};



module.exports.check = function(req , res){
	var response = {};
	var id = req.session.iouserid;
	 if (req.session.iologgedin || req.session.iologgedin != undefined){
	 	  //req.session.iocookie.maxAge = new Date(Date.now() + 3600000);
	 	  //req.session.iocookie.expires = new Date(Date.now() + 3600000);
	 	  console.log("iologged "+ req.session.iologgedin);
		  response = { iologgedin: req.session.iologgedin , type : req.session.ioaccountype, userid : req.session.iouserid, level : req.session.iolevel, name : req.session.ioname};
		  res.send(response);
	  }else{
	  	  console.log("iounlogged "+ req.session.iologgedin);
	  	  response = { iologgedin: false};
	  	  req.session.destroy();
	      res.send(response);
	  }
};

module.exports.getServices = function(req, res){
	connection.query('SELECT * FROM status', function(err, results){
		var response = {};
		if(err){
			console.log('ERROR initialization ipad : %s', err);
			response = { status : "ERROR" };
		}else{
			response = { status : "SUCCESS" , result : results};
		}
		res.send(response);
	});
};
